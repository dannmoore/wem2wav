# wem2wav

### Overview:

wem2wav is a small tool to convert AudioKinetic's Wwise .wem format files to standard .wav files.  wem2wav is compatible with Python 3.0 or above.

### Usage:

For a simple GUI run "python.exe wem2wav.py"

Alternatively you may specify the input and output filenames on the command line by running "python.exe wem2wav.py <infile> <outfile>"


### Notes:

Wwise files must be in either 44Khz or 22Khz and PCM codec.  Files must be individual sounds - sound banks are not supported.


### Legal:
Copyright (C) 2024 Dann Moore.  All rights reserved.




